package io.blocko.coinstack_example.c3;

import java.io.IOException;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.Endpoint;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.BlockchainStatus;
import io.blocko.coinstack.model.CredentialsProvider;

public class GettingStarted {

	public static void main(String[] args) throws IOException, CoinStackException {
		CoinStackClient client = new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "YOUR_COINSTACK_ACCESS_KEY";
			}

			@Override
			public String getSecretKey() {
				return "YOUR_COINSTACK_SECRET_KEY";
			}
		}, Endpoint.MAINNET);

		BlockchainStatus status = client.getBlockchainStatus();
		System.out.println("bestHeight: " + status.getBestHeight());
		System.out.println("bestBlockHash: " + status.getBestBlockHash());

		client.close();
	}
}
