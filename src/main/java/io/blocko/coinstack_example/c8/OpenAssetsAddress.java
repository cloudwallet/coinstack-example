package io.blocko.coinstack_example.c8;

import java.io.IOException;

import io.blocko.bitcoinj.core.AddressFormatException;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.openassets.util.Util;

public class OpenAssetsAddress {

	public static void main(String[] args) throws IOException, CoinStackException, AddressFormatException {
		String assetAddress = Util.deriveAssetAddressFromPrivateKey("YOUR_PRIVATE_KEY");
		System.out.println("asset address from private key: " + assetAddress);
		
		assetAddress = Util.deriveAssetAddressFromAddress("YOUR_WALLET_ADDRESS");
		System.out.println("asset address from wallet address: " + assetAddress);
		
		String derivedWalletAddress = Util.deriveAddressFromAssetAddress(assetAddress);
		System.out.println("original wallet address: " + "YOUR_WALLET_ADDRESS");
		System.out.println("wallet address from asset address: " + derivedWalletAddress);
	}
}
