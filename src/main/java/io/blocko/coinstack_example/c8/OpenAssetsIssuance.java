package io.blocko.coinstack_example.c8;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.ECKey;
import io.blocko.coinstack.openassets.ColoringEngine;
import io.blocko.coinstack.openassets.model.AssetOutput;
import io.blocko.coinstack.openassets.util.Util;
import io.blocko.coinstack_example.ClientFactory;

public class OpenAssetsIssuance {
	public static void main(String[] args) throws Exception {

		CoinStackClient client = ClientFactory.create();
		ColoringEngine coloringEngine = new ColoringEngine(client);

		// generate an asset address
		String yourAssetAddress = Util.deriveAssetAddressFromAddress("YOUR_WALLET_ADDRESS");

		long assetAmount = 1000;
		long fee = io.blocko.coinstack.Math.convertToSatoshi("0.0001");

		// sign at an issue asset transaction
		String rawTx = coloringEngine.issueAsset("YOUR_PRIVATE_KEY", assetAmount, yourAssetAddress, fee);

		// send the transaction
		client.sendTransaction(rawTx);

		// get remaining assets
		AssetOutput[] assets = coloringEngine.getUnspentAssetOutputs(yourAssetAddress);

		String assetId = "";
		System.out.println("Print Unspent Assets");
		for (AssetOutput assetOutput : assets) {
			System.out.println(assetOutput.getAssetID() + ": " + assetOutput.getAssetAmount());
			assetId = assetOutput.getAssetID();
		}

		// get a balance of an given assetId
		long remaining = coloringEngine.getAssetBalance(yourAssetAddress, assetId);
		System.out.println("Remaining Assets before Transfer: " + remaining);

		// generate a random address
		String newPrivateKeyWIF = ECKey.createNewPrivateKey();
		String targetAssetAddress = Util.deriveAssetAddressFromPrivateKey(newPrivateKeyWIF);

		assetAmount = 400;

		// transfer to the random address
		rawTx = coloringEngine.transferAsset("YOUR_PRIVATE_KEY", assetId, assetAmount,
				targetAssetAddress, fee);

		client.sendTransaction(rawTx);

		// print remaining balances
		remaining = coloringEngine.getAssetBalance(yourAssetAddress, assetId);
		System.out.println("Remaining Assets after Transfer: " + remaining);
		
		remaining = coloringEngine.getAssetBalance(targetAssetAddress, assetId);
		System.out.println("Assets to the Target: " + remaining);
	}
}
