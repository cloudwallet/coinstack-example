package io.blocko.coinstack_example.c6;

import java.io.IOException;

import io.blocko.apache.commons.codec.binary.Hex;
import io.blocko.bitcoinj.core.Sha256Hash;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Stamp;
import io.blocko.coinstack_example.ClientFactory;

public class GetStampStatus {
	
	static final String stampId = "2577d84dc7f29c7d2372f1b4ed579c7dd4f4c1c21f3167c0971e61c3aab9ddba-0";

	public static void main(String[] args) throws IOException, CoinStackException {
		CoinStackClient client = ClientFactory.create();

		// get stamp info
		Stamp stamp = client.getStamp(stampId);
		
		System.out.println("Stamp's Transaction ID: " + stamp.getTxId());
		System.out.println("Confirmation #: " + stamp.getConfirmations());
		System.out.println("Output Index: " + stamp.getOutputIndex());
		System.out.println("Timestamp: " + stamp.getTimestamp());
		
		//verify document
		String message = "Hello, blockchain!";
		Sha256Hash hash = Sha256Hash.create(message.getBytes());
		String originalHash = Hex.encodeHexString(hash.getBytes());
		
		String retreivedHash = stamp.getHash();
		
		if(retreivedHash.equals(originalHash)) {
			System.out.println("Document is Valid");
		} else {
			System.out.println("Document is Modified!");
		}
		
		client.close();
	}
}
