package io.blocko.coinstack_example.c6;

import java.io.IOException;

import io.blocko.apache.commons.codec.binary.Hex;
import io.blocko.bitcoinj.core.Sha256Hash;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack_example.ClientFactory;

public class StampDocument {

	public static void main(String[] args) throws IOException, CoinStackException {
		CoinStackClient client = ClientFactory.create();


		String message = "Hello, blockchain!";
		Sha256Hash hash = Sha256Hash.create(message.getBytes());
		String stampId = client.stampDocument("YOUR_PRIVATE_KEY", Hex.encodeHexString(hash.getBytes()));
		System.out.println("stampId: " + stampId);

		client.close();
	}
}
