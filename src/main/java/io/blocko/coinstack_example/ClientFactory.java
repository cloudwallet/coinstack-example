package io.blocko.coinstack_example;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.Endpoint;
import io.blocko.coinstack.model.CredentialsProvider;

public class ClientFactory {

	public static CoinStackClient create() {
		CoinStackClient client = new CoinStackClient(new CredentialsProvider() {
			@Override
			public String getAccessKey() {
				return "YOUR_COINSTACK_ACCESS_KEY";
			}

			@Override
			public String getSecretKey() {
				return "YOUR_COINSTACK_SECRET_KEY";
			}
		}, Endpoint.MAINNET);
		
		return client;
	}
}
