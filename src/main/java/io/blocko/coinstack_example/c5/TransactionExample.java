package io.blocko.coinstack_example.c5;

import java.io.IOException;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.ECKey;
import io.blocko.coinstack.TransactionBuilder;
import io.blocko.coinstack.TransactionUtil;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Transaction;
import io.blocko.coinstack_example.ClientFactory;

public class TransactionExample {

	public static void main(String[] args) throws IOException, CoinStackException {
		CoinStackClient client = ClientFactory.create();

		// create a target address to send
		String toPrivateKeyWIF = ECKey.createNewPrivateKey();
		String toAddress = ECKey.deriveAddress(toPrivateKeyWIF);

		// create a transaction
		long amount = io.blocko.coinstack.Math.convertToSatoshi("0.0002");
		long fee = io.blocko.coinstack.Math.convertToSatoshi("0.0001");

		TransactionBuilder builder = new TransactionBuilder();
		
		builder.addOutput(toAddress, amount);
		builder.setFee(fee);
		builder.allowDustyOutput(true);
		builder.setData("DATA_AT_OP_RETURN".getBytes());

		// sign the transaction
		String signedTx = client.createSignedTransaction(builder, "YOUR_PRIVATE_KEY");
		System.out.println(signedTx);
		
		// send the signed transaction
		client.sendTransaction(signedTx);
		

		String txId = TransactionUtil.getTransactionHash(signedTx);
		
		// print transaction
		Transaction tx = client.getTransaction(txId);
		System.out.println(tx.getConfirmationTime());

	}

}
