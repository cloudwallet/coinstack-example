package io.blocko.coinstack_example.c5;

import java.io.IOException;

import io.blocko.apache.commons.codec.binary.Hex;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.ECKey;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Output;
import io.blocko.coinstack_example.ClientFactory;

public class AddressExample {

	public static void main(String[] args) throws IOException, CoinStackException {
		CoinStackClient client = ClientFactory.create();

		// create a new private key
		String newPrivateKeyWIF = ECKey.createNewPrivateKey();
		System.out.println("private key: " + newPrivateKeyWIF);

		// derive a public key
		String newPublicKey = Hex.encodeHexString(ECKey.derivePubKey(newPrivateKeyWIF));
		System.out.println("public key: " + newPublicKey);

		// derive an address
		String your_wallet_address = ECKey.deriveAddress(newPrivateKeyWIF);
		System.out.println("address: " + your_wallet_address);

		// get a remaining balance
		long balance = client.getBalance(your_wallet_address);
		System.out.println("balance: " + balance);

		// print all transactions of a given wallet address
		String[] transactionIds = client.getTransactions(your_wallet_address);
		System.out.println("transactions");
		for (String txId : transactionIds) {
			System.out.println(txId);
		}

		// print all utxos
		Output[] outputs = client.getUnspentOutputs(your_wallet_address);
		System.out.println("unspent outputs");
		for (Output utxo : outputs) {
			System.out.println(utxo.getValue());
		}
	}

}
