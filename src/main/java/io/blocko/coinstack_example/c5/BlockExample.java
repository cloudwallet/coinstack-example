package io.blocko.coinstack_example.c5;

import java.io.IOException;

import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.model.Block;
import io.blocko.coinstack_example.ClientFactory;

public class BlockExample {

	public static void main(String[] args) throws IOException, CoinStackException {

		CoinStackClient client = ClientFactory.create();

		Block block = client.getBlock("0000000000000000017363cc0f6562f4e4552b94032c49ce37f5fb1309062fc6");

		System.out.println("blockId: " + block.getBlockId());
		System.out.println("parentId: " + block.getParentId());
		System.out.println("height: " + block.getHeight());
		System.out.println("time: " + block.getBlockConfirmationTime());
		
		client.close();
	}

}
