package io.blocko.coinstack_example.c7;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.blocko.apache.commons.codec.DecoderException;
import io.blocko.apache.commons.codec.binary.Hex;
import io.blocko.bitcoinj.core.ScriptException;
import io.blocko.bitcoinj.script.Script;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.Endpoint;
import io.blocko.coinstack.MultiSig;
import io.blocko.coinstack.TransactionBuilder;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack.exception.MalformedInputException;
import io.blocko.coinstack_example.ClientFactory;

public class MultisignFundingExample {

	// generate a P2SH address
	public static String createAddressFromRedeemScript(String redeemScript, boolean isMainNet)
			throws MalformedInputException {
		try {
			Script redeem = new Script(Hex.decodeHex(redeemScript.toCharArray()));
			String from = MultiSig.createAddressFromRedeemScript(redeem, isMainNet);

			return from;
		} catch (ScriptException e) {
			throw new MalformedInputException("Invalid redeem script", "Parsing redeem script failed");
		} catch (DecoderException e) {
			throw new MalformedInputException("Invalid redeem script", "Parsing redeem script failed");
		}
	}

	public static void main(String[] args) throws IOException, CoinStackException, DecoderException {

		CoinStackClient client = ClientFactory.create();

		String publickey1 = "04a5d638e12e184485f85c9159a45040afdb7a303380a3bd002742cb929d5484e1d6f792977f03e3bba23a65fde28c76dd0f90a9e2f0d528f7108f70270c48a744";
		String publickey2 = "0444cccc489d6a9e3b815156a8d647b8e613bad580c3e81485eea86447a5eb2a86ce514d0cd425545195819e8bc35b04b24fbfbe23badd610e8d2a8e8e1c6db354";
		String publickey3 = "043311ef6ef1e133ea612d235a473351bc4a96e5393140b67d711ad396cbed233e03fdc77890775df7af002b1bdcb61a04fb15623c23571d2235448ae12a2c6429";

		List<byte[]> publickeys = new ArrayList<byte[]>(3);

		publickeys.add(Hex.decodeHex(publickey1.toCharArray()));
		publickeys.add(Hex.decodeHex(publickey2.toCharArray()));
		publickeys.add(Hex.decodeHex(publickey3.toCharArray()));

		String redeemScript = MultiSig.createRedeemScript(2, publickeys);
		System.out.println("Redeem Script: " + redeemScript);

		String multiAddress = createAddressFromRedeemScript(redeemScript, Endpoint.MAINNET.mainnet());
		System.out.println("Mixed Multiple Address: " + multiAddress);
		System.out.println("Remaing Balance Before Charging: Sender=" + client.getBalance("YOUR_WALLET_ADDRESS")
				+ ", MultiSigUser=" + client.getBalance(multiAddress));

		// send money
		long amount = io.blocko.coinstack.Math.convertToSatoshi("0.0003");
		long fee = io.blocko.coinstack.Math.convertToSatoshi("0.0001");

		TransactionBuilder prepareBuilder = new TransactionBuilder();
		prepareBuilder.addOutput(multiAddress, amount);
		prepareBuilder.setFee(fee);

		// sign the transaction
		String prepareSignedTx = client.createSignedTransaction(prepareBuilder, "YOUR_PRIVATE_KEY");

		// send the signed transaction
		client.sendTransaction(prepareSignedTx);
		System.out.println("Remaing Balance After Charging: Sender=" + client.getBalance("YOUR_WALLET_ADDRESS")
				+ ", MultiSigUser=" + client.getBalance(multiAddress));

	}
}
