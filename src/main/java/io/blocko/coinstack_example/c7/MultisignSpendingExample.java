package io.blocko.coinstack_example.c7;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.blocko.apache.commons.codec.DecoderException;
import io.blocko.coinstack.CoinStackClient;
import io.blocko.coinstack.TransactionBuilder;
import io.blocko.coinstack.exception.CoinStackException;
import io.blocko.coinstack_example.ClientFactory;

public class MultisignSpendingExample {

	public static void main(String[] args)
			throws IOException, CoinStackException, DecoderException, InterruptedException {
		CoinStackClient client = ClientFactory.create();

		// assign generated values at the previous example
		String redeemScript = "5241043311ef6ef1e133ea612d235a473351bc4a96e5393140b67d711ad396cbed233e03fdc77890775df7af002b1bdcb61a04fb15623c23571d2235448ae12a2c6429410444cccc489d6a9e3b815156a8d647b8e613bad580c3e81485eea86447a5eb2a86ce514d0cd425545195819e8bc35b04b24fbfbe23badd610e8d2a8e8e1c6db3544104a5d638e12e184485f85c9159a45040afdb7a303380a3bd002742cb929d5484e1d6f792977f03e3bba23a65fde28c76dd0f90a9e2f0d528f7108f70270c48a74453ae";
		String multiAddress = "3BR3SLniskKxSiw955dH96sRws28Yabq8e";

		String privateKey1 = "5JgE3WaFAxaynSMb4fQP8Gyd9c6jsB56BGKWEoCTdasetUJXHQb";
		String privateKey2 = "5KP7PfjDN7qiMyTe759D3WdhPWSNSidP9ZnDEPrm3un7ZWp4q2T";
		String privateKey3 = "5KNa4RjCdwv85ftCfdTHCEn9kZjBbsviuYVbHrQsvdLKLyVbbv1";

		String sendToAddress = "YOUR_WALLET_ADDRESS";
		long amount = io.blocko.coinstack.Math.convertToSatoshi("0.0002");
		long fee = io.blocko.coinstack.Math.convertToSatoshi("0.0001");
		
		TransactionBuilder builder = new TransactionBuilder();
		builder.addOutput(sendToAddress, amount);
		builder.setFee(fee);

		// sign using only 2 keys of 3
		List<String> prikeys = new ArrayList<String>();
		prikeys.add(privateKey1);
		prikeys.add(privateKey3);

		String signedTx = client.createMultiSigTransaction(builder, prikeys, redeemScript);

		// send transaction
		client.sendTransaction(signedTx);

		System.out.println("Remaing Balance After Spending: MultiSigUser=" + client.getBalance(multiAddress)
				+ ", Sender=" + client.getBalance(sendToAddress));
	}
}
